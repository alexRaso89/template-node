var express = require('express');
var router = express.Router();
const debug=require('debug')('prove-node:indexRouter');
/* GET home page. */
router.get('/', function(req, res, next) {
 debug("sono in index router");
  res.json( { title: 'Express' });
});
router.post('/prova/:name', function(req,res,next){
  debug("sono in post");
  res.status(200).json({
      param: {
      query:req.query,
      params:req.params,
      body:req.body
      }}
      );
  
  });
module.exports = router;
