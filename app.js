var express = require('express');
var path = require('path');
var logger = require('morgan');
const debug=require('debug')('prove-node:app');
var indexRouter = require('./routes/index');
const helmet = require("helmet");
const probe = require('kube-probe');
var app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

probe(app);
app.use(helmet());

app.use(function(req,res, next){
    debug('sono in function');
    res.locals.data="pippo";
    next();
})
app.use('/api', indexRouter);
/*app.post('/:name', function(req,res,next){
debug("sono in post");
res.status(200).json({
    param: {
    query:req.query,
    params:req.params,
    body:req.body
    }}
    );

});*/
app.use(function(req,res,next){
    debug('sono in not found');
    const error=new Error("not found");
    error.status=404;
    next(error);

});
app.use(function(err,req,res,next){
    
    res.status(err.status).json({error:err.message});

});
module.exports = app;
